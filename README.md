# CocoaPodLibrary

[![CI Status](https://img.shields.io/travis/vetri/CocoaPodLibrary.svg?style=flat)](https://travis-ci.org/vetri/CocoaPodLibrary)
[![Version](https://img.shields.io/cocoapods/v/CocoaPodLibrary.svg?style=flat)](https://cocoapods.org/pods/CocoaPodLibrary)
[![License](https://img.shields.io/cocoapods/l/CocoaPodLibrary.svg?style=flat)](https://cocoapods.org/pods/CocoaPodLibrary)
[![Platform](https://img.shields.io/cocoapods/p/CocoaPodLibrary.svg?style=flat)](https://cocoapods.org/pods/CocoaPodLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CocoaPodLibrary is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CocoaPodLibrary'
```

## Author

vetri, vetri@mediamelon.com

## License

CocoaPodLibrary is available under the MIT license. See the LICENSE file for more info.
