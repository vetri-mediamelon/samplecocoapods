#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "objc_mmsmartstreaming.h"
#import "ReachabilityMM.h"

FOUNDATION_EXPORT double CocoaPodLibraryVersionNumber;
FOUNDATION_EXPORT const unsigned char CocoaPodLibraryVersionString[];

