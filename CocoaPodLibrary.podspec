Pod::Spec.new do |s|
  s.name             = 'CocoaPodLibrary'
  s.version          = '1.0.0'
  s.summary          = 'My First sample cocoapod'
  s.description      = 'My First sample cocoapod for MMSmartstreaming'

  s.homepage         = 'https://bitbucket.org/vetri-mediamelon/samplecocoapods'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'vetri' => 'vetri@mediamelon.com' }
  s.source           = { :git => 'https://bitbucket.org/vetri-mediamelon/samplecocoapods.git', :tag => '1.0.0' }
  s.swift_versions   = '4.2'

  s.ios.deployment_target = '8.0'
  s.source_files = 'CocoaPodLibrary/Classes/**/*.{h,swift}'
  #s.exclude_files = 'CocoaPodLibrary/Classes/MMSmartStreaming/*.plist"'
  

  #s.public_header_files = 'CocoaPodLibrary/Classes/**/*.h'
  s.frameworks = 'UIKit', 'AVFoundation', 'CoreTelephony'
  s.vendored_libraries = 'CocoaPodLibrary/Classes/MMSmartStreaming/libmmsmartstreamer.a'
  s.libraries = 'stdc++'
  s.xcconfig = {'OTHER_LDFLAGS' => '-ObjC',
      'HEADER_SEARCH_PATHS' =>'$(inherited) ${PODS_ROOT}/#{s.name}/Classes/MMSmartStreaming/ $(PODS_ROOT)',
      'ARCHS' => '$(inherited)',
      'VALID_ARCHS' =>'$(inherited)',
      'SWIFT_INCLUDE_PATHS' => '${PODS_ROOT}/#{s.name}/Classes/MMSmartStreaming/'
      
  }
  s.pod_target_xcconfig = { 'SWIFT_INCLUDE_PATHS' => '${PODS_ROOT}/#{s.name}/Classes/MMSmartStreaming/',
      'HEADER_SEARCH_PATHS' => '$(PODS_ROOT)/CocoaPodLibrary/Classes/MMSmartStreaming'
  }

  #s.header_mappings_dir = 'CocoaPodLibrary/Classes/'
  #s.public_header_files = 'CocoaPodLibrary/Classes/MMSmartStreaming/*.h'
  #s.requires_arc     = true

  #s.module_name = 'CocoaPodLibrary'
  #s.module_map = 'CocoaPodLibrary/Classes/MMSmartStreaming/module.modulemap'
  #s.preserve_path = 'CocoaPodLibrary/Classes/MMSmartStreaming/module.modulemap'

end
